import subprocess, os, tempfile, time, shlex, signal
from subprocess import Popen, PIPE, STDOUT

CMD_SHELL = 'cmd.exe'

CMD_COMMAND = [ '\"C:\\Python27\\python27.exe\"'
              , '\"C:\\Users\\fezna&fredo\\Git\\python-exercise\\print.py\"'
              ]
              
CMD_COMMAND_LINE = 'C:\\Python27\\python27.exe \
                  \"C:\\Users\\fezna&fredo\\Git\\python-exercise\\print.py\"  \n'

print (CMD_COMMAND)

fd, f_path = tempfile.mkstemp( suffix = ".txt"
                             , text=True
                             , dir = 'C:\\Temp' 
                             )

print (fd, f_path)

# command_line = input()

# CMD_COMMAND = shlex.split(command_line)

# print (CMD_COMMAND)

with open(f_path, 'wt') as writer:
    try:
        process = Popen( CMD_COMMAND
                         # CMD_SHELL
                       , stdout = writer
                       , stderr = writer
                       # , stdin = PIPE
                       # , universal_newlines = True
                       )
    except  (RuntimeError, TypeError, NameError):
        print ("An error occured")
    
    print (process, process.pid, process.poll())
    
    # process.communicate(input = CMD_COMMAND_LINE)

    input("Press Enter to terminate")
    
    process.communicate(input = '\n')
    
    while process.poll() == None:
        process.kill()
        process.wait()
        time.sleep(1)
            
print (process, process.pid, process.poll())

with open(f_path, 'r', 1) as reader:
    for line in reader:
        line = line.rstrip()
        print (line
              #, end=""
              )                   
                   
os.close(fd)
os.remove(f_path)