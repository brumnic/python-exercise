import subprocess, os, tempfile, time
from subprocess import Popen, PIPE, STDOUT

file_path = os.path.dirname(os.path.realpath(__file__))

curr_wrk_dir_path = os.getcwd()

print (file_path, curr_wrk_dir_path)

CMD_SHELL = 'cmd.exe'

CMD_COMMAND ='dir \"C:\\Users\\fezna&fredo\\Git\\udemy-python\\Section_06\" \n'

print (CMD_COMMAND)

fd, f_path = tempfile.mkstemp( suffix = ".txt"
                             , text=True
                             , dir = 'C:\\Temp' 
                             )

print (fd, f_path)

with open(f_path, 'w') as writer:
    process = Popen( CMD_SHELL
                   , stdout = writer
                   , stdin = PIPE
                   , universal_newlines = True
                   )
    
    print (process, process.pid, process.poll())
    
    process.communicate(input = CMD_COMMAND)    
    
    print (process, process.pid, process.poll())
    
    while process.poll() == None:
        process.kill()
        process.wait()    
        
with open(f_path, 'r', 1) as reader:
    for line in reader:
        line = line.rstrip()
        print (line
              #, end=""
              )                   
                   
os.close(fd)
os.remove(f_path)