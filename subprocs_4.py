import subprocess
import time
import ctypes
import signal, os

devnull = open(os.devnull, 'w')

GenerateConsoleCtrlEvent = ctypes.windll.kernel32.GenerateConsoleCtrlEvent
answer = raw_input ("Enter the number of seconds to run (Parent): ")
proc = subprocess.Popen( "Python27.exe print.py"
                       , stdin=subprocess.PIPE
                       #, stdout=devnull
                       )

proc.stdin.write("%d\n" %proc.pid)
proc.stdin.write("%d\n" %int(answer))
time.sleep(5) # just so it runs for a while


print "\nsending ctrl c"
try:
    GenerateConsoleCtrlEvent(signal.CTRL_C_EVENT, proc.pid)
    # GenerateConsoleCtrlEvent(signal.CTRL_C_EVENT, 0)
    #os.kill(proc.pid, signal.CTRL_C_EVENT)
    proc.wait()
except KeyboardInterrupt:
    print "ignoring ctrl c"

print "still running"