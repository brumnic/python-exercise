import subprocess
import sys
import time


PYTHON = sys.executable
SCRIPT = __file__


def main(name):
    print('%s started' % name)

    # A and B spawn a subprocess
    if name == 'A':
        child = subproc('B')
    elif name == 'B':
        child = subproc('C')
    else:
        child = None

    # Sleep and wait for a Ctrl=C
    try:
        time.sleep(10)
        print('%s done' % name)
    except KeyboardInterrupt:
        print('%s got KeyboardInterrupt' % name)
    finally:
        if child:
            child.wait()


def subproc(name):
    """Create and return a new subprocess named *name*."""
    proc = subprocess.Popen([PYTHON, SCRIPT, name])
    return proc


if __name__ == '__main__':
    name = sys.argv[1] if len(sys.argv) > 1 else 'A'
    main(name)