import os, tempfile, subprocess, time, signal
def signal_handler(signal, frame):
    time.sleep(1)
    print ('Ctrl+C received in execution.py')
cwd_path = os.getcwd()
fd, f_path = tempfile.mkstemp(suffix=".txt", dir = cwd_path, text = True)
signal.signal(signal.SIGINT, signal_handler)
with open(f_path, 'w') as file:
    process = subprocess.Popen("python27 wrapper.py", shell=True, stdout = file)
    time.sleep(30)
    os.kill(process.pid, signal.CTRL_C_EVENT)