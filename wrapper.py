#wrapper.py
import subprocess, time, signal, sys, os, tempfile

def signal_handler(signal, frame):
    time.sleep(1)
    print 'Ctrl+C received in wrapper.py'

cwd_path = os.getcwd()
fd, f_path = tempfile.mkstemp(suffix=".txt", dir = cwd_path, text = True)    
    
signal.signal(signal.SIGINT, signal_handler)
print "wrapper.py started"
time.sleep(10)
with open(f_path, 'w') as file:
    process = subprocess.Popen("jupyter notebook")
    time.sleep(20) #Replace with your IPC code here, which waits on a fire CTRL-C request
    process.sendsignal(signal.CTRL_C_EVENT)
    time.sleep(20)
    process.sendsignal(signal.CTRL_C_EVENT)
    time.sleep(20)
    os.kill(process.pid, signal.CTRL_C_EVENT)