#demo.py

import signal, sys, time

def signal_handler(signal, frame):
    print 'Ctrl+C received in demo.py'
    time.sleep(1)
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
print 'demo.py started'
#signal.pause() # does not work under Windows
while(True):
    time.sleep(1)